# README #

This project is a simple prototype of a core Javascript / HTML5 Canvas alarm clock.

### What is this repository for? ###

* Coded to satisty the requirements of the "Front-End Interview Project" for SquareSpace.
* V 1.0.0 beta

### How do I get set up? ###

* Unzip the project
* Run the project from a local web server pointed to file-path-to-project/deploy/
* If the project is going to be transpiled from source located in file-path-to-project/src/, run npm install to get the necessary packages and dependencies
* Requires NPM to install all dependencies. And a live internenet connection for Google Fonts.

### Given more time, I would... ###

* Validate the input's entries to not accept non digits.
* List the set alarms and allow to unset them from this UI (It is already possible to unset them in code.)
* Write tests
* Document the code
* Port the model and notifications to Redux