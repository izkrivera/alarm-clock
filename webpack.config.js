const webpack = require("webpack"),
	  path = require("path"),
	  OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin"),
	  ExtractTextPlugin = require("extract-text-webpack-plugin"),
	  extractSass = new ExtractTextPlugin({
    	filename: "./css/clock.css", 
		allChunks: true,
    	disable: process.env.production === "false"
	  });

module.exports = (env) => {
	console.info("(***) Webpack compilation env:", env);
	return {
		context: 	__dirname,
		devtool: 	env.production === "true" ? "nosources-source-map" : "eval-source-map",
		entry: 		{ clock: "./src/js/clock.js" },
		resolve: 	{ extensions: [".js", ".jsx", ".json", ".scss"] },
		module: 	{
			rules: 	[
				{
					test   : /\.(js|jsx)$/,
					exclude: /(node_modules|bower_components )/,
					use : [ { 
						loader: "babel-loader",
						options  : {
							presets: ["react", "es2015", "es2016", "es2017", "stage-0", "stage-1"],
							plugins: ["react-html-attrs", "transform-decorators-legacy", "transform-runtime"]
						}
					} ]
				},
				{
					test: /\.css$/,
					exclude: /(node_modules|bower_components)/,
					use: ExtractTextPlugin.extract({ use: 'css-loader' })
				},
				{
					test: /\.scss$/,
					use: extractSass.extract({
						use: [
							{ loader: "css-loader" }, 
							{ loader: "sass-loader" }
						],
						fallback: "style-loader"
					})
				},
				{
					test: /\.(json)$/,
					exclude: /(node_modules|bower_components)/,
					use: [ { loader: "json-loader" } ]
				}
			]
		},
		target: 	"web",
		output: {
			path: path.join(__dirname, "deploy"),
			filename: "./js/[name].bundle.js"
		},
		plugins: env.production === "true" ? [
			new webpack.optimize.UglifyJsPlugin(),
			extractSass,
			new OptimizeCssAssetsPlugin({
				assetNameRegExp: /css\/clock\.css$/g,
				cssProcessor: require("cssnano"),
				cssProcessorOptions: { discardComments: { removeAll: true } },
				canPrint: true
			})
		] : [ extractSass ]
	};
};
