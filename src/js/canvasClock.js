const requestAnimationFrame =   window.requestAnimationFrame || 
                                window.mozRequestAnimationFrame ||
                                window.webkitRequestAnimationFrame || 
                                window.msRequestAnimationFrame;

const cancelAnimationFrame =    window.cancelAnimationFrame || 
                                window.mozCancelAnimationFrame || 
                                window.webkitCancelAnimationFrame || 
                                window.msCancelAnimationFrame;

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class Size {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }
}

class Rect {
    constructor(x, y, width, height) {
        this.origin = new Point(x, y);
        this.size = new Size(width, height);
    }

    get center() { return new Point(this.midX, this.midY ) }

    get x() { return this.origin.x }

    get y() { return this.origin.y }

    get width() { return this.size.width }

    get height() { return this.size.height }

    get midX() { return this.x + this.width / 2 }

    get midY() { return this.y + this.height / 2 }
}

const defaultStyle = {
    foregroundColor: '#666',
    backgroundColor: 'white',
    accentColor: '#035',
    alarmColor: '#0c0',
    alarmFiringColor: '#c00'
};

const renderClock = (canvas, scale = 0.9, style, alarms = []) => {

        const ctx = canvas.getContext('2d');
        const now = new Date();
        const canvasRect = new Rect(0, 0, canvas.width, canvas.height);
        const diameter = Math.min(canvasRect.width, canvasRect.height) * scale;

        const clearCanvas = () => {
            ctx.save();
            ctx.clearRect(canvasRect.x, canvasRect.y, canvasRect.width, canvasRect.height);
            ctx.restore();
        };

        const drawOuterRing = () => {
            ctx.save();
            ctx.fillStyle = style.foregroundColor;
            ctx.strokeStyle = style.accentColor;
            ctx.beginPath();
            ctx.arc(canvasRect.center.x, canvasRect.center.y, diameter / 2, 0, Math.PI * 2);
            ctx.fill();
            ctx.stroke();

            ctx.fillStyle = style.backgroundColor;
            ctx.beginPath();
            ctx.arc(canvasRect.center.x, canvasRect.center.y, diameter / 2 * scale, 0, Math.PI * 2);
            ctx.fill();
            ctx.stroke();
            ctx.restore();
        };

        const drawCenterDisc = () => {
            ctx.save();
            ctx.beginPath();
            ctx.lineWidth = 2;
            ctx.fillStyle = style.foregroundColor;
            ctx.strokeStyle = style.accentColor;
            ctx.arc(canvasRect.center.x, canvasRect.center.y, diameter / 35, 0, Math.PI * 2);
            ctx.stroke();
            ctx.fill();
            ctx.restore();
        };

        const drawHourMarks = () => {
            
            ctx.save();
            ctx.strokeStyle = style.foregroundColor;
            ctx.lineWidth = 2;
            
            const length = diameter / 2 * scale * scale;

            for (var i = 0; i < 12; i++) {
                const angle = (i - 3) * (Math.PI * 2) / 12;
                const x1 = canvasRect.center.x + Math.cos(angle) * length;
                const y1 = canvasRect.center.y + Math.sin(angle) *length;
                const x2 = canvasRect.center.x + Math.cos(angle) * (length - length / 7);
                const y2 = canvasRect.center.y + Math.sin(angle) * (length - length / 7);

                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x2, y2);
                ctx.stroke();
            }

            ctx.restore();
        };

        const drawSecondMarks = () => {
            
            ctx.save();
            ctx.strokeStyle = style.foregroundColor;
            ctx.lineWidth = 2;
            
            const length = diameter / 2 * scale * scale;

            for (var i = 0; i < 60; i++) {
                const angle = (i - 3) * (Math.PI * 2) / 60;
                const x1 = canvasRect.center.x + Math.cos(angle) * length;
                const y1 = canvasRect.center.y + Math.sin(angle) * length;
                const x2 = canvasRect.center.x + Math.cos(angle) * (length - length / 30);
                const y2 = canvasRect.center.y + Math.sin(angle) * (length - length / 30);

                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x2, y2);
                ctx.stroke();
            }

            ctx.restore();
        };

        const drawAlarms = () => {
            
            ctx.save();
            
            const length = diameter / 2 * scale * 0.95;

            for (var i = 0; i < alarms.length; i++) {
                const alarm = alarms[i];
                const { hour, minute, fired } = alarm;

                ctx.fillStyle = fired ? style.alarmFiringColor : style.alarmColor;

                const angle = ((hour + minute / 60) * Math.PI / 6) - (Math.PI * 2 / 4);
                const x = canvasRect.center.x + Math.cos(angle) * length;
                const y = canvasRect.center.y + Math.sin(angle) * length;

                ctx.beginPath();
                ctx.moveTo(x, y);
                ctx.arc(x, y, 6, 0, Math.PI * 2);
                ctx.fill();
            }

            ctx.restore();
        };

        const drawSecondsHand = () => {
            
            ctx.save();
            ctx.lineWidth = 2;
            ctx.strokeStyle = style.foregroundColor; 
           
            const sec = now.getSeconds();
            const angle = (Math.PI * 2 * (sec / 60)) - (Math.PI * 2 / 4);
            const length = diameter / 2 * scale * scale;
            
            ctx.beginPath();
            ctx.moveTo(canvasRect.center.x, canvasRect.center.y);
            ctx.lineTo(
                canvasRect.center.x + Math.cos(angle) * length,
                canvasRect.center.y + Math.sin(angle) * length
            );

            ctx.moveTo(canvasRect.center.x, canvasRect.center.y);
            ctx.lineTo(
                canvasRect.center.x - Math.cos(angle) * 60,
                canvasRect.center.y - Math.sin(angle) * 60
            );

            ctx.stroke();
            ctx.restore();
        };

        const drawMinutesHand = () => {

            ctx.save();
            ctx.strokeStyle = style.foregroundColor;
            ctx.lineWidth = 4;

            const min = now.getMinutes();
            const angle = (Math.PI * 2 * (min / 60)) - (Math.PI * 2 / 4);
            const length = diameter / 2 * scale * scale;

            ctx.beginPath();
            ctx.moveTo(canvasRect.center.x, canvasRect.center.y);
            ctx.lineTo(
                canvasRect.center.x + Math.cos(angle) * length / 1.1, 
                canvasRect.center.y + Math.sin(angle) * length / 1.1
            );

            ctx.stroke();
            ctx.restore();
        };

        const drawHoursHand = () => {
            
            ctx.save();
            ctx.strokeStyle = style.foregroundColor;
            ctx.lineWidth = 8;
           
            const hour = now.getHours();
            const min = now.getMinutes();
            const angle = (Math.PI * 2 * (hour * 5 + min / 60 * 5) / 60) - (Math.PI * 2 / 4);
            const length = diameter / 2 * scale * scale * 0.8;

            ctx.beginPath();
            ctx.moveTo(canvasRect.center.x, canvasRect.center.y);

            ctx.lineTo(
                canvasRect.center.x + Math.cos(angle) * length, 
                canvasRect.center.y + Math.sin(angle) * length
            );
            
            ctx.stroke();
            ctx.restore();
        };

        clearCanvas();
        drawOuterRing();
        drawCenterDisc();
        drawHourMarks();
        drawSecondMarks();
        drawAlarms();
        drawSecondsHand();
        drawMinutesHand();
        drawHoursHand();
    };

class CanvasClock {
    
    constructor(canvasId, scale, style = defaultStyle, alarms = []) {
        
        this.canvasId = canvasId;
        this.style = style;
        this.scale = scale;
        this.alarms = [];

        for (var i = 0; i < alarms.length; i++) {
            this.addAlarm(alarms[i]);
        }

        this.canvas = document.getElementById(canvasId);
        this.animating = false;
        this.animationFrame = null;
    }

    setAlarm(alarm) {
        if (Alarm.isAlarm(alarm)) {
            var alarmAlreadySet = false;
            this.alarms.forEach((a) => {
                if (a.equals(alarm)) { alarmAlreadySet = true }
            });
            if (!alarmAlreadySet) { this.alarms.push(alarm) }
        }
    }

    unsetAlarm(alarm) {
        this.alarms = this.alarms.filter((a) => { return !a.equals(alarm) });
    }

    checkAlarms() {

        if (this.alarms.length === 0) { return }

        const self = this;
        const now = new Date();
        const nowAlarm = new Alarm(
            now.getHours(),
            now.getMinutes()
        );

        this.alarms.forEach((a) => {
            if (a.equals(nowAlarm) && !a.fired) {
                a.fired = true;
                if (a.callback && typeof a.callback === 'function') {
                    a.callback(a);
                }
            } else if(!a.equals(nowAlarm) && a.fired) {
                a.fired = false;
            }
        });
    }

    start() {
        this.animating = true;
        const self = this;
        const tick = () => {
            if (self.animating) {
                self.checkAlarms();
                const { canvas, scale, style, alarms } = self;
                renderClock(canvas, scale, style, alarms);
                self.animationFrame = requestAnimationFrame(tick);
            }
        };
        requestAnimationFrame(tick);
    }

    stop() {
        this.animating = false;
        if (this.animationFrame != null) {
            cancelAnimationFrame(this.animationFrame);
            this.animationFrame = null;
        }
    }
}

class Alarm {
    constructor(hour, minute, callback) {
        this.hour = hour > 12 ? hour - 12 : hour;
        this.minute = minute;
        this.fired = false;
        this.callback = callback;
    }

    equals(alarm) {
        return (
            alarm === this || (
                Alarm.isAlarm(alarm) && 
                alarm.hour === this.hour && 
                alarm.minute === this.minute
            )
        );
    }

    static isAlarm(alarm) { return (alarm && alarm instanceof Alarm) }
}

export default CanvasClock;
export { Alarm };