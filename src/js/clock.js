require('../scss/clock.scss');

const { 
	canvasId, 
	clockScale, 
	clockStyle,
	addAlarmButtonId,
	hourInputId,
	minuteInputId,
	alarmSound
} = require('../data/config.json');

import CanvasClock, { Alarm } from './canvasClock';

const clock = new CanvasClock(canvasId, clockScale, clockStyle);
clock.start();

const addAlarmButton = document.getElementById(addAlarmButtonId);
const hourInput = document.getElementById(hourInputId);
const minuteInput = document.getElementById(minuteInputId);

addAlarmButton.addEventListener('click', (event) => {
	const hour = parseInt(hourInput.value, 10) || 0;
	const minute = parseInt(minuteInput.value, 10) || 0;

	hourInput.value = null;
	minuteInput.value = null;

	const sound = new Audio(alarmSound);
	sound.load();
	const alarm = new Alarm(hour, minute, (a) => { sound.play() });
	clock.setAlarm(alarm);
});
